﻿namespace Server.Controllers
{
    using System;
    using System.Web.Mvc;

    public class ApplicationController : Controller
    {
        public new ActionResult View()
        {
             if (IsApiRequest())
             {
                 return new HttpStatusCodeResult(200);
             }

// ReSharper disable Asp.NotResolved
            return base.View();
// ReSharper restore Asp.NotResolved
        }

        public new ActionResult View(object model)
        {
            if (IsApiRequest())
            {
                return new JsonResult {Data = model, JsonRequestBehavior = JsonRequestBehavior.AllowGet};
            }

// ReSharper disable Asp.NotResolved
            return base.View(model);
// ReSharper restore Asp.NotResolved
        }

        public bool IsApiRequest()
        {
            try
            {
                var key = Request.Headers["X-API"];
                if (key != null && key.ToUpperInvariant() == "JSON")
                {
                    return true;
                }

                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }
    }
}