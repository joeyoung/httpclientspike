﻿namespace Server.Controllers
{
    using System;
    using System.Web.Mvc;
    using Models.Home;

    public class HomeController : ApplicationController
    {
        [HttpGet]
        public ActionResult Index()
        {
            var model = new IndexViewModel {ServerDate = DateTime.Now, ServerMessage = "Hello from the Server"};

            return View(model);
        }
        
        [HttpPost]
        public ActionResult Index(IndexViewModel model)
        {
            model.ServerDate = DateTime.Now;
            model.ServerMessage = String.Format("Hello {0}", model.Name);

            return View(model);
        }
    }
}