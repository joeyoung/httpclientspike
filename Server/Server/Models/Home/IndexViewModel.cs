﻿namespace Server.Models.Home
{
    using System;

    public class IndexViewModel
    {
        public string Name { get; set; }
        public string API { get; set; }

        public DateTime ServerDate { get; set; }

        public string ServerMessage { get; set; }
    }
}