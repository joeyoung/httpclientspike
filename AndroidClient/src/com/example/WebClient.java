package com.example;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

public class WebClient {

    private List<NameValuePair> parameters;
    private List<NameValuePair> headers;
    private int responseCode;
    private String responseString;
    private String responseMessage;

    public WebClient() {
        parameters = new ArrayList<NameValuePair>();
        headers = new ArrayList<NameValuePair>();
    }

    public void addParameter(String name, String value) {
        parameters.add(new BasicNameValuePair(name, value));
    }

    public void addHeader(String name, String value) {
        headers.add(new BasicNameValuePair(name, value));
    }

    public void clear() {
        clearHeaders();
        clearParameters();
        responseCode = 0;
        responseString = "";
        responseMessage = "";
    }

    public void clearParameters() {
        parameters.clear();
    }

    public void clearHeaders() {
        headers.clear();
    }

    public String getResponse() {
        return responseString;
    }

    public String getResponseMessage() {
        return responseMessage;
    }

    public int getResponseCode() {
        return responseCode;
    }
    public String get(String url) throws Exception{
        // turn the parameters into a querystring
        String queryString = "";
        if (!parameters.isEmpty()) {
            queryString += "?";
            for(NameValuePair pair : parameters) {
                String parameter = pair.getName() + "=" + URLEncoder.encode(pair.getValue(), "UTF-8");
                if (queryString.length() > 1) {
                    queryString += "&" + parameter;
                } else {
                    queryString += parameter;
                }
            }
        }

        HttpGet request = new HttpGet(url + queryString);
        for(NameValuePair pair : headers) {
            request.addHeader(pair.getName(), pair.getValue());
        }

        executeRequest(request);

        return responseString;
    }

    public int post(String url) throws Exception{
        HttpPost request = new HttpPost(url);
        for(NameValuePair pair : headers) {
            request.addHeader(pair.getName(), pair.getValue());
        }

        if (!parameters.isEmpty()) {
            request.setEntity(new UrlEncodedFormEntity(parameters, "UTF-8"));
        }

        executeRequest(request);

        return responseCode;
    }

    private void executeRequest(HttpUriRequest request) throws Exception{
        HttpClient client = new DefaultHttpClient();

        HttpResponse response;

        try {
            response = client.execute(request);
            responseCode = response.getStatusLine().getStatusCode();
            responseMessage = response.getStatusLine().getReasonPhrase();

            HttpEntity entity = response.getEntity();
            if (entity != null) {
                InputStream stream = entity.getContent();
                responseString = convertStreamToString(stream);
                stream.close();
            }

        } catch (ClientProtocolException ex) {
            client.getConnectionManager().shutdown();
            ex.printStackTrace();
            throw ex;
        } catch(IOException ex) {
            client.getConnectionManager().shutdown();
            ex.printStackTrace();
            throw ex;
        }
    }

    private static String convertStreamToString(InputStream is) {

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder sb = new StringBuilder();

        String line;

        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                is.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return sb.toString();
    }
}
