package com.example;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MyActivity extends Activity
{
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        final Button btnGet = (Button)findViewById(R.id.btnGet);
        final Button btnPost = (Button)findViewById(R.id.btnPost);
        final EditText txtData = (EditText)findViewById(R.id.txtData);
        final TextView txtResponse = (TextView)findViewById(R.id.txtResponse);
        final EditText txtUrl = (EditText)findViewById(R.id.txtUrl);

        btnGet.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                WebClient client = new WebClient();
                client.addHeader("X-API", "JSON");
                try {
                    String response = client.get(txtUrl.getText().toString());
                    txtResponse.setText(response);
                } catch (Exception ex) {
                    txtResponse.setText(ex.getMessage());
                }
            }
        });
    }
}
